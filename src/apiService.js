import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'https://virtserver.swaggerhub.com/StanislavVojir/CIMPLE/1.0',
  headers: {
    'Content-Type': 'application/json',
  },
});

export const getClaims = async (status, factchecker, offset = 0, limit = -1) => {
  try {
    const response = await apiClient.get('/claims', {
      params: { status, factchecker, offset, limit },
    });
    return response.data;
  } catch (error) {
    console.error('Error fetching claims:', error);
    throw error;
  }
};

// Experts related to a claim
export const getClaimExperts = async (claimId) => {
  try {
    const response = await apiClient.get(`/claims/${claimId}/experts`);
    return response.data;
  } catch (error) {
    console.error(`Error fetching experts for claim ${claimId}:`, error);
    throw error;
  }
};

// Evidences related to a claim
export const getClaimEvidences = async (claimId) => {
  try {
    const response = await apiClient.get(`/claims/${claimId}/evidences`);
    return response.data;
  } catch (error) {
    console.error(`Error fetching evidences for claim ${claimId}:`, error);
    throw error;
  }
};

// Related claims
export const getRelatedClaims = async (claimId) => {
  try {
    const response = await apiClient.get(`/claims/${claimId}/related`);
    return response.data;
  } catch (error) {
    console.error(`Error fetching related claims for claim ${claimId}:`, error);
    throw error;
  }
};

// Evidences
export const getEvidences = async () => {
  try {
    const response = await apiClient.get('/evidences');
    return response.data;
  } catch (error) {
    console.error('Error fetching evidences:', error);
    throw error;
  }
};

// Experts
export const getExperts = async () => {
  try {
    const response = await apiClient.get('/experts');
    return response.data;
  } catch (error) {
    console.error('Error fetching experts:', error);
    throw error;
  }
};

// Users
export const getUsers = async () => {
  try {
    const response = await apiClient.get('/users');
    return response.data;
  } catch (error) {
    console.error('Error fetching users:', error);
    throw error;
  }
};