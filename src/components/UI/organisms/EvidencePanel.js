import React, { useState, useEffect } from "react";
import { EvidenceRecord } from "../molecules/EvidenceRecord";
//import evidence from "../../../mocks/evidence.json";
import { getEvidences } from "../../../apiService";
import { Stack, Text, Icon, useDisclosure } from "@chakra-ui/react";
import { BsArrowsAngleExpand } from "react-icons/bs";
import { EvidencePanelDetail } from "./EvidencePanelDetail";

export const EvidencePanel = ({ alreadyOpen }) => {
  const [evidence, setEvidence] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchEvidence = async () => {
      try {
        const data = await getEvidences();
        setEvidence(data);
      } catch (error) {
        console.error('Error fetching evidence:', error);
      } finally {
        setLoading(false);
      }
    };

    fetchEvidence();
  }, []);

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [selectedEvidence, setSelectedEvidence] = useState([]);

  useEffect(() => {
    const storedEvidence =
      JSON.parse(localStorage.getItem("selectedEvidence")) || [];
    setSelectedEvidence(storedEvidence);
  }, []);

  const handleEvidenceSelectionToggle = (id) => {
    setSelectedEvidence((prevSelectedEvidence) => {
      const updatedEvidence = prevSelectedEvidence.includes(id)
        ? prevSelectedEvidence.filter((recordId) => recordId !== id)
        : [...prevSelectedEvidence, id];

      localStorage.setItem("selectedEvidence", JSON.stringify(updatedEvidence));
      return updatedEvidence;
    });
  };

  if (loading) return <p>Loading...</p>;

  return (
    <Stack
      direction="column"
      justify="flex-start"
      align="flex-start"
      spacing="16px"
      flex="1"
      alignSelf="stretch"
    >
      <Stack
        paddingX={alreadyOpen ? "0px" : "35px"}
        paddingY={alreadyOpen ? "0px" : "25px"}
        borderRadius={alreadyOpen ? "0px" : "12px"}
        direction="column"
        justify="flex-start"
        align="flex-start"
        spacing="16px"
        overflow="hidden"
        flex="1"
        alignSelf="stretch"
        background="#FFFFFF"
      >
        {alreadyOpen === true ? null : (
          <Stack
            direction="row"
            justify="space-between"
            align="flex-start"
            spacing="16px"
            alignSelf="stretch"
          >
            <Stack
              direction="row"
              justify="flex-start"
              align="center"
              spacing="10px"
            >
              <Stack
                direction="row"
                justify="flex-start"
                align="center"
                spacing="8px"
              >
                <Text
                  fontFamily="Inter"
                  lineHeight="1.43"
                  fontWeight="semibold"
                  fontSize="14px"
                  color="#2B6CB0"
                >
                  Evidence
                </Text>
              </Stack>
            </Stack>
            <Icon as={BsArrowsAngleExpand} onClick={onOpen} />
          </Stack>
        )}
        <EvidencePanelDetail
          isOpen={isOpen}
          onOpen={onOpen}
          onClose={onClose}
        />
        <Stack
          direction="column"
          justify="flex-start"
          align="flex-start"
          spacing="0"
          alignSelf="stretch"
        >
          {evidence.map((evidenceItem) => (
            <EvidenceRecord
              key={evidenceItem.id}
              id={evidenceItem.id}
              title={evidenceItem.title}
              date={evidenceItem.date}
              author={evidenceItem.author}
              text={evidenceItem.text}
              type={evidenceItem.type}
              onEvidenceSelectionToggle={handleEvidenceSelectionToggle}
              isSelectedEvidence={selectedEvidence.includes(evidenceItem.id)}
            />
          ))}
        </Stack>
      </Stack>
    </Stack>
  );
};
