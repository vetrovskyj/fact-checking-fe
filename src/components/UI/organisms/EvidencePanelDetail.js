import { ModalTemplate } from "../molecules/ModalTemplate";
import { EvidencePanel } from "../organisms/EvidencePanel";
import React from "react";

export const EvidencePanelDetail = ({ isOpen, onClose }) => {
  return (
    <ModalTemplate title="Evidence" isOpen={isOpen} onClose={onClose}>
      <EvidencePanel alreadyOpen={isOpen} />
    </ModalTemplate>
  );
};
