import { Stack, Text } from "@chakra-ui/react";
import { AvatarAtom } from "../atoms/AvatarAtom";
//import users from '../../../mocks/users.json';
import React, { useState, useEffect } from "react";
import { getUsers } from "../../../apiService";

export const UserProfile = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const data = await getUsers();
        setUsers(data);
      } catch (error) {
        console.error("Error fetching users:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchUsers();
  }, []);

  const user = users.find(user => user.id === 10);

  if (loading) return <p>Loading...</p>;

  return (
    <Stack direction="row" justify="flex-start" align="center" spacing="12px">
      <AvatarAtom firstName={user.firstName} lastName={user.lastName} />
      <Stack
        direction="column"
        justify="flex-start"
        align="flex-start"
        spacing="0px"
      >
        <Text
          fontFamily="Inter"
          lineHeight="1.5"
          fontWeight="regular"
          fontSize="16px"
          color="#000000"
          textAlign="center"
        >
          {user.firstName + " " + user.lastName}
        </Text>
        <Text
          fontFamily="Inter"
          lineHeight="1.33"
          fontWeight="regular"
          fontSize="12px"
          color="#414243"
          textAlign="center"
        >
          {user.role}
        </Text>
      </Stack>
    </Stack>
  );
};
